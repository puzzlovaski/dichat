package main

import "net"
import "fmt"
import "bufio"
import "os"

func connect(ip string) net.Conn{
	
	fmt.Println("CONNECTING...")
	
	conn, err := net.Dial("tcp", ip)              //Connect to this address:port
	if err != nil {
		fmt.Println("ANABLE TO CONNECT")
		os.Exit(0)
	}

	fmt.Println("CONNECTED, Exit with Ctrl+C")
	return conn
}

func listen(port string) net.Conn {

	ln, err := net.Listen("tcp", ":" + port)      //Open on this :port
	if err != nil {
		fmt.Println("UNABLE TO MAKE LISTENER")
	}

	
	fmt.Println("LISTENNING...")
	
	conn, err := ln.Accept()                      //Waiting for connection
	if err != nil {
		fmt.Println("UNABLE TO LISTEN")
	}
	
	fmt.Println("CONNECTED, Exit with Ctrl+C")

	return conn
}

func recv(conn net.Conn){

	reader := bufio.NewReader(conn)               //Message receiver
	
	for{
		message, err := reader.ReadString('\n')   //Receive until new line
		if err != nil {
			fmt.Println("UNABLE TO RECEIVE")
			os.Exit(0)
		}
		
		fmt.Print("-: "+message)                  //Print received message
	}
}

func send(conn net.Conn){

	reader := bufio.NewReader(os.Stdin)           //Keyboard input receiver
	
	for{
		text, err := reader.ReadString('\n')      //Read until new line
		if err != nil {
			fmt.Println("UNABLE TO READ")
			os.Exit(0)
		}
		
		fmt.Fprintf(conn, text)                   //Sent message to connection
	}
}

func man(){
	
	fmt.Println(
		"Usage: dichat <command>\n",
		"\n",
		"Simple TCP chatting program. \n",
		"\n",
		"Commands:\n",
		"    connect <address>:<port>    Connect to specific address.\n",
		"    listen <port>               Listening on specific port.")
	
}

func main() {
	
	var conn net.Conn

	if len(os.Args) <= 1 {                        //Explain how to use
		
		man()
		os.Exit(0)
		
	} else if os.Args[1] == "connect" {           //Connecting
		
		conn = connect(os.Args[2])
		
	} else if os.Args[1] == "listen" {            //Listening
		
		conn = listen(os.Args[2])
		
	} else {                                      //Explain how to use
		
		man()
		os.Exit(0)
		
	}
	
	go recv(conn)                                 //Thread for receive
	
	send(conn)                                    //Thread for send(main thread)
}
